package com.example.demo.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.data.entity.Folder;

public interface FolderRepository extends JpaRepository<Folder, String> {
    Folder findByFoldername(String name);
}
