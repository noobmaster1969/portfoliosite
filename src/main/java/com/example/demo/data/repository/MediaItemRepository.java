package com.example.demo.data.repository;

import com.example.demo.data.entity.Folder;
import com.example.demo.data.entity.MediaItem;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Set;

public interface MediaItemRepository extends JpaRepository<MediaItem, String> {
    Page<MediaItem> findAll(Pageable pageable);
    List<MediaItem> findAllByOrderByPublicationDateAsc();
    List<MediaItem> findByFolder(Folder folder);
    List<MediaItem> findByIdIn(Set<String> keys, Pageable pageable);
}
