package com.example.demo.data.repository;

import com.example.demo.data.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, String> {
    List<Tag> findTop10BySearchStringContaining(String query);
}
