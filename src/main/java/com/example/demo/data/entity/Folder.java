package com.example.demo.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
public class Folder {
    @Id
    private String id;
    private String foldername;

    @JsonIgnore
    @OneToMany
    private List<MediaItem> mediaItemList;

    @PrePersist
    void generatieID() { id = UUID.randomUUID().toString(); }
    public String getId() { return id; }

    public String getFoldername() { return foldername; }
    public void setFoldername(String foldername) { this.foldername = foldername; }

    public List<MediaItem> getMediaItemList() { return mediaItemList; }
    public void addToMediaItemList(MediaItem mediaItem) { this.mediaItemList.add(mediaItem); }
}
