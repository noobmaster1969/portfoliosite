package com.example.demo.data.entity;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.servlet.http.HttpServletRequest;

@Entity
public class User {

    // Single value attributes
    @Id
    private String id;
    private String username;
    private String firstname;
    private String lastname;
    private String email;
    private AccessToken.Access realmAccess;
    private AccessToken accessToken;

    public User(){}

    public User(HttpServletRequest request){
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
        KeycloakPrincipal principal=(KeycloakPrincipal)token.getPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        accessToken = session.getToken(); // Token
        username = accessToken.getPreferredUsername(); // Gebruikersnaam
        id = accessToken.getId(); // Id in keycloak
        firstname = accessToken.getGivenName(); // Voornaam
        lastname = accessToken.getFamilyName(); // Achternaam
        email = accessToken.getEmail(); // Emailadress
        realmAccess = accessToken.getRealmAccess(); // Realm
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getFirstname() { return firstname; }
    public void setFirstname(String firstname) { this.firstname = firstname; }

    public String getLastname() { return lastname; }
    public void setLastname(String lastname) { this.lastname = lastname; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public AccessToken.Access getRealmAccess() { return realmAccess; }

    public AccessToken getAccessToken() {return accessToken;}
}
