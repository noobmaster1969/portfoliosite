package com.example.demo.data.entity;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Tag {

    // Single value attributes
    @Id
    private String id;

    @Lob
    @Column(columnDefinition = "text")
    private String searchString;

    public Tag(){}

    public Tag(MediaItem item){
        super();
        this.id = item.getId();
        this.searchString = item.getTitle().toLowerCase() + "; ";
    }

    // Getters and setters
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public String getSearchString() { return searchString; }
    public void setSearchString(String searchString) { this.searchString = searchString; }

}
