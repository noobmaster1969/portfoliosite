package com.example.demo.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Entity
public class MediaItem {

    // Single value attributes
    @Id
    private String id;
    private String title;
    private String source;
    private String filetype;
    private String publicationDate;

    @ManyToOne
    private Folder folder;

    @PrePersist
    void generatieIDandPublicationdate() {
        id = UUID.randomUUID().toString();
        publicationDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    }

    public String getId() { return id; }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public String getSource() { return source; }
    public void setSource(String source) { this.source = source; }

    public String getFiletype() { return filetype; }
    public void setFiletype(String filetype) { this.filetype = filetype; }

    public Folder getFolder() { return folder; }
    public void setFolder(Folder folder) { this.folder = folder; }

    public String getPublicationDate() { return publicationDate; }
}
