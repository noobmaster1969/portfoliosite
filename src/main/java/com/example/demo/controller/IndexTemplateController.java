package com.example.demo.controller;

import com.example.demo.data.entity.Folder;
import com.example.demo.data.entity.MediaItem;
import com.example.demo.data.entity.User;
import com.example.demo.service.*;
import org.bouncycastle.math.raw.Mod;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.context.LazyContextVariable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.demo.config.ImageSourceConfigurer.uploadDirectory;


@Controller
public class IndexTemplateController {

    @Autowired
    private FolderService folderService;
    @Autowired
    private MediaItemService mediaService;

    @GetMapping
    public String Index(Model model, @RequestParam("page")Optional<Integer> page, @RequestParam("size")Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        // Get media
        List<MediaItem> latestItems = mediaService.getLatest();
        Page<MediaItem> mediaPage = mediaService.findPaginated(PageRequest.of(currentPage-1, pageSize),latestItems);

        // Get all folders
        List<Folder> allFolders = folderService.getAll();

        // Make models
        model.addAttribute("allFolders", allFolders);
        model.addAttribute("lastMedia", mediaPage);
        model.addAttribute("pageTitle", "Latest");

        // Get pagenumbers and set model
        int totalPages = mediaPage.getTotalPages();
        if(totalPages > 0){
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "index";
    }

    @GetMapping("/folder")
    public String folder(Model model, @RequestParam(name="fold", required=true, defaultValue="") String fold, @RequestParam("page")Optional<Integer> page, @RequestParam("size")Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        // Get media
        Page<MediaItem> mediaPage = null;
        Folder folder = folderService.getByName(fold);
        if(folder != null){
            mediaPage = mediaService.findPaginated(PageRequest.of(currentPage-1, pageSize),folder.getMediaItemList());
        }

        // Get all folders
        List<Folder> allFolders = folderService.getAll();

        // Set models
        model.addAttribute("allFolders", allFolders);
        model.addAttribute("lastMedia", mediaPage);
        model.addAttribute("pageTitle", fold);

        // Get pagenumbers and set model
        int totalPages = mediaPage.getTotalPages();
        if(totalPages > 0){
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "index";
    }

    @GetMapping("/search")
    public String search(Model model, @RequestParam(name="srom", required = true, defaultValue = "") String srom, @RequestParam("page")Optional<Integer> page, @RequestParam("size")Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);

        // Get media
        Page<MediaItem> mediaPage = mediaService.findPaginated(PageRequest.of(currentPage-1, pageSize),mediaService.searchItems(srom.toLowerCase()));

        // Get all folders
        List<Folder> allFolders = folderService.getAll();

        // Set models
        model.addAttribute("lastMedia", mediaPage);
        model.addAttribute("allFolders", allFolders);
        model.addAttribute("pageTitle", srom);

        // Get pagenumbers and set model
        int totalPages = mediaPage.getTotalPages();
        if(totalPages > 0){
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "index";
    }

    @GetMapping("/upload")
    public String upload(Model model){
        model.addAttribute("title", new String());
        model.addAttribute("folders", folderService.getAll());
        return "upload";
    }

    // TODO: Fix fout in attribute title
    @PostMapping("/upload")
    public String upload(Model model, HttpServletRequest request, @RequestParam("file") MultipartFile file, @RequestParam String title, @RequestParam String fold, RedirectAttributes attributes){
        KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();

        if(principal != null){
            String userId = principal.getAccount().getKeycloakSecurityContext().getIdToken().getSubject();
        }

        // check if file is empty
        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "redirect:/upload";
        }

        //upload file

        model.addAttribute("folder", folderService.getOne(fold).getFoldername());
        model.addAttribute("title", title);
        model.addAttribute("filename", file.getOriginalFilename());
        return "testupload";
    }

    @GetMapping("/user")
    public String user(Model model, HttpServletRequest request){
        User user = new User(request);
        model.addAttribute("pageTitle", "Welcome "+user.getUsername());
        return "user";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) throws ServletException{
        request.logout();
        return "redirect:/";
    }

    @GetMapping("/account")
    public String account(){
        return "redirect:http://192.168.2.8:8080/auth/realms/devlab/userinfo/";
    }

    @GetMapping("/login")
    public String login(){
        return "redirect:/";
    }
}
