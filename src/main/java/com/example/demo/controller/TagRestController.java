package com.example.demo.controller;

import com.example.demo.data.entity.Tag;
import com.example.demo.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/media/tag")
public class TagRestController {
    @Autowired
    private TagService tagService;

    @GetMapping("/all")
    public List<Tag> getAll(){return tagService.getAll();}
}
