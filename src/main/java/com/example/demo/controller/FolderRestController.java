package com.example.demo.controller;

import com.example.demo.data.entity.Folder;
import com.example.demo.data.entity.MediaItem;
import com.example.demo.service.FolderService;
import com.example.demo.service.MediaItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/folder")
public class FolderRestController {
    @Autowired
    private FolderService folderService;
    @Autowired
    private MediaItemService mediaService;

    @GetMapping("/all")
    public List<Folder> getAll(){ return folderService.getAll(); }

    @PostMapping
    public Folder saveOne(@RequestBody Folder folder){
        folderService.createFolder(folder);
        return folder;
    }

    @GetMapping("/{id}")
    public Folder getOne(@PathVariable String id){ return folderService.getOne(id); }

    @GetMapping("/{id}/media")
    public List<MediaItem> getMediaFromFolder(@PathVariable String id){ return folderService.getOne(id).getMediaItemList(); }

    @PostMapping("/{id}/media/{mediaId}/add")
    public Folder addMediaToFolder(@PathVariable String id, @PathVariable String mediaId){
        Folder tempFolder = folderService.getOne(id);
        folderService.addMediaToFolder(tempFolder, mediaId);
        return tempFolder;
    }
}
