package com.example.demo.controller;

import com.example.demo.data.entity.Folder;
import com.example.demo.data.entity.MediaItem;
import com.example.demo.data.entity.Tag;
import com.example.demo.service.FolderService;
import com.example.demo.service.MediaItemService;
import com.example.demo.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/media")
public class MediaItemRestController {
    @Autowired
    private MediaItemService mediaService;
    @Autowired
    private TagService tagService;

    //@GetMapping("/all")
    //public List<MediaItem> getAll(){ return mediaService.getAll(); }

    @PostMapping
    public MediaItem saveOne(@RequestBody MediaItem item){
        mediaService.saveItem(item);
        return item;
    }

    @GetMapping("/{id}")
    public MediaItem getOne(@PathVariable String id){return mediaService.getOne(id);}

    @GetMapping("/{id}/tag")
    public Tag getTag(@PathVariable String id){return tagService.getTag(id);}

}
