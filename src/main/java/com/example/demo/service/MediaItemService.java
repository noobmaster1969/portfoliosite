package com.example.demo.service;

import com.example.demo.data.entity.MediaItem;
import com.example.demo.data.entity.Tag;
import com.example.demo.data.repository.MediaItemRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class MediaItemService {
    @Autowired
    private MediaItemRepository mediaRepo;
    @Autowired
    private TagService tagService;

    private Pageable firstPageWithTwentyElements = PageRequest.of(0, 20);


    public List<MediaItem> getAll(){return mediaRepo.findAll();}

    public void saveItem(MediaItem item) {
        item.setFiletype(FilenameUtils.getExtension(item.getSource()));
        mediaRepo.save(item);
        Tag newTag = new Tag(item);
        tagService.saveOne(newTag);
    }
    public MediaItem getOne(String id){ return mediaRepo.getOne(id); }

    public List<MediaItem> getLatest(){
        List<MediaItem> latest = mediaRepo.findAllByOrderByPublicationDateAsc();
        return latest;
    }

    public List<MediaItem> searchItems(String query){
        Set<String> keys = tagService.getVideoSelection(query);
        return mediaRepo.findByIdIn(keys, firstPageWithTwentyElements);
    }

    public Page<MediaItem> findPaginated(Pageable pageable, List<MediaItem> allMediaItems){
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<MediaItem> mediaList;
        if(allMediaItems.size() < startItem){
            mediaList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allMediaItems.size());
            mediaList = allMediaItems.subList(startItem, toIndex);
        }
        Page<MediaItem> mediaItemPage
                = new PageImpl<MediaItem>(mediaList, PageRequest.of(currentPage, pageSize), allMediaItems.size());

        return mediaItemPage;
    }

}
