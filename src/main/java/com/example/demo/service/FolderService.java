package com.example.demo.service;

import com.example.demo.data.entity.Folder;
import com.example.demo.data.entity.MediaItem;
import com.example.demo.data.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FolderService {
    @Autowired
    private FolderRepository folderRepo;
    @Autowired
    private MediaItemService mediaService;
    @Autowired
    private TagService tagService;

    public List<Folder> getAll() { return folderRepo.findAll(); }

    public void createFolder(Folder folder){folderRepo.save(folder);}

    public void addMediaToFolder(Folder folder, String itemId) {
        MediaItem tempItem = mediaService.getOne(itemId);
        folder.addToMediaItemList(tempItem);
        tempItem.setFolder(folder);
        mediaService.saveItem(tempItem);
        folderRepo.save(folder);
        tagService.addToTag(tempItem.getId(), folder.getFoldername());
    }
    public Folder getOne(String id) { return folderRepo.getOne(id); }
    public Folder getByName(String name){ return folderRepo.findByFoldername(name); }
}
