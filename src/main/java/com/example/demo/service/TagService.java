package com.example.demo.service;

import com.example.demo.data.entity.Tag;
import com.example.demo.data.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TagService {
    @Autowired
    private TagRepository tagRepo;


    public Tag getTag(String id){ return tagRepo.getOne(id); }

    public void saveOne(Tag newTag){ tagRepo.save(newTag); }

    public void changeTag(String id, String newTagString){
        Tag oldTag = tagRepo.getOne(id);
        oldTag.setSearchString(newTagString);
        tagRepo.save(oldTag);
    }
    public void addToTag(String id, String addString){
        Tag oldTag = tagRepo.getOne(id);
        oldTag.setSearchString(oldTag.getSearchString()+addString+";");
        tagRepo.save(oldTag);
    }
    Set<String> getVideoSelection(String query){
        return tagRepo.findTop10BySearchStringContaining(query).stream().map(Tag::getId).collect(Collectors.toSet());
    }

    public List<Tag> getAll() {
        return tagRepo.findAll();
    }
}
