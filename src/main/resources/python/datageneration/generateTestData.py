import random
import uuid
from datetime import date
import os

today = date.today()
print("Today is: ", today.strftime("%d-%m-%Y"))

folders = ["Funny", "School", "Zuyd", "Dank"]
files = [["Funny/1567367644000.jpg","Funny/1567369237000.jpg","Funny/AAAAH.mp4","Funny/A-Cate-riding-a-Water-Horn.jpg", "Funny/Accurate_2.jpg"], ["Funny/1567367644000.jpg","Funny/1567369237000.jpg","Funny/AAAAH.mp4","Funny/A-Cate-riding-a-Water-Horn.jpg", "Funny/Accurate_2.jpg"], ["Funny/1567367644000.jpg","Funny/1567369237000.jpg","Funny/AAAAH.mp4","Funny/A-Cate-riding-a-Water-Horn.jpg", "Funny/Accurate_2.jpg"], ["Funny/1567367644000.jpg","Funny/1567369237000.jpg","Funny/AAAAH.mp4","Funny/A-Cate-riding-a-Water-Horn.jpg", "Funny/Accurate_2.jpg"], ["Funny/1567367644000.jpg","Funny/1567369237000.jpg","Funny/AAAAH.mp4","Funny/A-Cate-riding-a-Water-Horn.jpg", "Funny/Accurate_2.jpg"]]

folderTemplate = """
INSERT INTO FOLDER (ID, FOLDERNAME) VALUES ('#id#', '#name#');
"""

fileTemplate = """
INSERT INTO MEDIA_ITEM (ID,FILETYPE,PUBLICATION_DATE,SOURCE,TITLE,FOLDER_ID) VALUES ('#id#', '#filetype#', '#pubDate#', '#source#', '#title#', '#folderId#');
"""

folderFileTemplate = """
INSERT INTO FOLDER_MEDIA_ITEM_LIST (FOLDER_ID,MEDIA_ITEM_LIST_ID) VALUES ('#folderId#', '#fileId#');
"""

tagTemplate = """
INSERT INTO TAG (ID,SEARCH_STRING) VALUES ('#Id#', '#searchstring#');
"""
filesFolderPlace = 0
with open('../../data/demodata.sql', 'w') as f:
    for folder in folders:
        folderId = str(uuid.uuid4())
        folder = [folderId,folder]

        print(folder)
        insertFolder = folderTemplate.replace("#id#", folder[0])
        insertFolder = insertFolder.replace("#name#", folder[1])
        f.write(insertFolder)

        singleFolder = []
        for x in range(0,5):
            singleFolder.append([str(uuid.uuid4()),folder[1]+"-file-"+str(x)])

        filePlace = 0
        for file in singleFolder:
            print(file)
            notneeded, file_extension = os.path.splitext(files[filesFolderPlace][filePlace])

            insertFile = fileTemplate.replace("#id#", file[0])
            insertFile = insertFile.replace("#pubDate#", str(today))
            insertFile = insertFile.replace("#source#", files[filesFolderPlace][filePlace])
            insertFile = insertFile.replace("#filetype#", file_extension)
            insertFile = insertFile.replace("#title#", file[1])
            insertFile = insertFile.replace("#folderId#", folder[0])
            f.write(insertFile)

            insertFolderFile = folderFileTemplate.replace("#folderId#", folder[0])
            insertFolderFile = insertFolderFile.replace("#fileId#", file[0])
            f.write(insertFolderFile)

            searchString = file[1]+"; "+folder[1]+"; "

            insertTag = tagTemplate.replace("#Id#", file[0])
            insertTag = insertTag.replace("#searchstring#", searchString.lower())
            f.write(insertTag)

            filePlace += 1

        filesFolderPlace += 1
        print()


